<?php

$error_messages['password_error'] = "Password does not match"; ///english
$error_messages['invalid_login_credentials'] = "Invalid login credentials"; ///english
$error_messages['parameter_missing'] = "Parameter missing"; ///english
$error_messages['authentication_failed'] = "Authentication Failed"; ///english
$error_messages['request_failed'] = "Request Failed"; ///english
$error_messages['success'] = "Success"; ///english
$error_messages['no_records_found'] = "No records found"; ///english
$error_messages['logout_success_msg'] = "You logged out successfully"; ///english
$error_messages['error_occured'] = "error occured"; ///english
$error_messages['email_exists'] = "Email already exists"; ///english
$error_messages['email_not_exists'] = "Email does not exists"; ///english
$error_messages['user_registered_successfully'] = "User registered successfully.";
$config['per_page'] = 20;
$error_messages['email_sent_user'] = "Email Sent to user."; ///english

$error_messages['order_created'] = "Order created successfully."; ///english

$param_mis = array(
    "status" => 200,
    "msg" => "parameter missing"
);

$param_mis_json = json_encode($param_mis);
?>